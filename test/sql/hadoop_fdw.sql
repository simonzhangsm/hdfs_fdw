-- The following pre-requisites should be met before starting
-- the test run
--
-- 1) --- USE STATIC DATA IN HIVE TABLES ---
-- The district table is used extensively in the tests.
-- It should have static data. The static data is stored in
-- fdw_dir/test/files/district_table_static_csv_dump.csv
-- Load this into the district table from Hive like so:
--
-- LOAD DATA LOCAL INPATH '/path/to/csvfile' OVERWRITE INTO TABLE district;
--
-- Same for the history table. The static data is stored in
-- fdw_dir/test/files/history_table_static_csv_dump.csv
--
-- LOAD DATA LOCAL INPATH '/path/to/csvfile' OVERWRITE INTO TABLE history;
--
-- 2) --- FLUME DEPENDENCIES ---
-- Test inserts into underlying HDFS files using Flume
-- NOTE: this requires a properly configured Flume instance
-- running before the start of this test run
-- The conf file required by Flume is available in
-- test/files/flume_hive_hdfs.conf
-- Here's how to start Flume. Change directory into your installed
-- bigsql location
-- cd bigsql-9.3prefix
-- Fire off the following command to start Flume with the above
-- conf file
--./flume/bin/flume-ng agent --conf ./flume/conf/ -f ~/path/to/this/fdw/test/files/flume_hive_hdfs.conf
-- -Dflume.root.logger=DEBUG,console -n test1
-- NOTE: This needs to be started before running "make installcheck"!!
-- 
-- 3) --- HBASE DEPENDENCIES ---
-- Test INSERTs, UPDATEs, DELETEs into underlying Hbase table
-- NOTE: this requires a properly configured Hbase instance
-- running before the start of this test run. The corresponding
-- thrift server also needs to be up and running
-- We need to create an Hbase mapped Hive table as well for this.
-- NOTE: The name of the mapped Hive table should be the same
-- as the underlying Hbase table. This is needed because the fdw
-- will use the same table name to query Hbase directly as well
-- NOTE: The same hbase mapping for the columns should be used
-- in the foreign table as the one present in the Hive mapped
-- table as well. Otherwise DML will not work properly at all
--
-- Create a hive table based on an Hbase one before starting
-- the test run. Go to the "hive" commandline and issue the below
-- Drop if it exists first.
-- "DROP TABLE hbase_table"
--
-- Issue CREATE command now
--
-- CREATE TABLE hbase_table(key string, value string) STORED BY 'org.apache.hadoop.hive.hbase.HBaseStorageHandler' WITH
-- SERDEPROPERTIES ("hbase.columns.mapping" = ":key,cf:val") TBLPROPERTIES ("hbase.table.name" = "hbase_table");
--
-- NO need to do anything in Hbase. The above will create the underlying Hbase table as well
-- NOTE: The run can HANG if things are not setup properly
--
--
-- START TEST RUN ---
CREATE EXTENSION hadoop_fdw;
CREATE SERVER localhive FOREIGN DATA WRAPPER hadoop_fdw OPTIONS (address '127.0.0.1', port '10000');

CREATE USER MAPPING FOR public server localhive;

CREATE FOREIGN TABLE district (
  d_w_id       integer,
  d_id         integer,
  d_ytd        numeric,
  d_tax        numeric,
  d_next_o_id  integer,
  d_name       varchar,
  d_street_1   varchar,
  d_street_2   varchar,
  d_city       varchar,
  d_state      varchar,
  d_zip        varchar
) SERVER localhive 
OPTIONS (table 'district');

CREATE FOREIGN TABLE history (
  h_c_id       integer,
  h_c_d_id     integer,
  h_c_w_id     integer,
  h_d_id       integer,
  h_w_id	   integer,
  h_date       timestamp,
  h_amount     float,
  h_data       varchar
) SERVER localhive
OPTIONS (table 'history');

-- General testing of queries

SELECT d_w_id, d_id, d_name FROM district ORDER BY d_w_id;

SELECT d_w_id, d_id, d_name FROM district WHERE d_w_id = 5;

SELECT d_w_id, d_id, d_name FROM district WHERE d_w_id < 5 ORDER BY d_w_id;

SELECT d_w_id, d_id, d_name FROM district WHERE d_w_id >= 5 ORDER BY d_w_id;

SELECT d_w_id, d_id, d_name FROM district WHERE d_w_id >=5 AND d_w_id < 10 ORDER BY d_w_id;

-- Set rowscanthreshold to 0 to allow sending WHERE clauses to Hive
-- Setting rowscanthreshold in some ways disables local clause checking
-- Set client_min_messages to debug1 to catch the query being sent to
-- Hive. In future a cleaner way would be to have a GUC to control which
-- messages from the FDW need to be sent back to the client. Setting
-- client_min_messages works for now
set hadoop_fdw.hadoop_rowscanthreshold to 0;
set client_min_messages to debug1;

-- Issue no 1: LIKEs not being pushed down to Hive

-- Check if LIKE is working
select * from district where d_name like '%Abl%';

-- Check if NOT LIKE is working
select * from district where d_name not like '%Abl%';


-- Issue no 4: IN/NOT IN not being pushed down to Hive

-- Check if IN is working
select * from district where d_name in ('AblnEVzO', 'PAiLA');

-- Check if NOT IN is working
select * from district where d_name not in ('AblnEVzO', 'PAiLA');

-- Issue no 5: Push functions in WHERE clauses to Hive

-- Check a standard mapped function
select * from district where upper(d_name) = 'ABLNEVZO';

-- Check a function which gets mapped to another function in Hive
select * from district where strpos(d_name, 'nE') = 4;

-- Check functions like ltrim which should be shipped in only
-- one argument form. Others should be evaluated locally

-- ltrim(one_arg) should be sent
select * from district where ltrim(d_name) = 'AblnEVzO';

-- ltrim(two_args) should NOT be
select * from district where ltrim(d_name, ' ') = 'AblnEVzO';

-- Check functions like lpad where we APPEND arguments while
-- sending to Hive. A ' ' should be appended below

select * from district where lpad(d_name, 20) = 'AblnEVzO';

-- Check functions like date_part which are totally transformed

select * from history where date_part('month', h_date) = 7;

-- Issue no 6: Gather stats and Analyze Hive tables

analyze district;

-- Check that reltuples is equal to 20 for this table.
-- This does not seem to be working right now
-- select reltuples from pg_class where relname = 'district';

-- Issue 7: Improve Remote Plans. If the table has rows less than
-- hadoop_fdw.hadoop_rowscanthreshold, then the WHERE clause should be
-- evaluated locally

reset hadoop_fdw.hadoop_rowscanthreshold;

-- district table has 20 rows, so with the above guc back to 10000
-- the WHERE clause should not be sent to Hive

select d_w_id, d_id, d_name from district where d_w_id = 5;

-- Issue no 8: Remote hard coded lengths
-- set guc to 0 to ensure that query is sent to Hive with WHERE
-- clause
set hadoop_fdw.hadoop_rowscanthreshold to 0;

select * from district where d_name = '8aShDF62wkhuTIjN4NO5BQgUFwDS9emHEEzSU5URqCMKU6hYT6deVK9lHMDQ10hFFhhimCAdNXxsdfR7l5lHOv3fIH5JHmZVUHFISPvqwtiZZAgLFSddOgJgwOzECYkgpyyIOu9LosuO3bjH4NuS4E91c8Foh0VXyUpOOyiDrdcuFMCJj8lnMuoy35nk5II4m8SB8BPzo1t5N6O8EAva5kY8pMsueByRJRcRc2QS3LWQRuYf6UGBFoJ5BC1pO0HhRt95uZWxu4OMyw25RIGg7ZlHlm7AmOrEH1JDapAVtZrsWtwwBDdImQZYDgiz5aEMbXZCNjgHJY9pR7mdKQu7qUf4CP4H0IdbpDnDxUUH2e6Uks75J3CAXrEjHH0HZesQrfdpA86DmDgX6ncPqoZOgn8x58Ffm85eniTxqZBdmrBsfnHWcquJe3HjBWPxeUbSD5Q4fbhSTrK9fcgHTba9ersqOIn2mQU0Vu4BWld0dxAJaqb5SCD747xRPlUCCyChtHsQ3WQgVa16RbBtnO1rUyJtkm6wlIefZY6c5XKZ8KfYwqSkFTcjSudDijAU3pAcOGFSnZ3vuiUqZvbnPEXr9C5rvFMy5WbSnrvbRxWLg1DGwo4M3cECnJ4kYQjdwL6kD2Le0r0hsDwp22D5dRHRkLCJlvwiH3TU5o85g8mZLjQOlcSP4jqo538qy5ZG83kDrsIY259NoYlaCE0Gxr54uEutIUBQXudPnvxp17DqfyRrDR8BJEFERA9keJBCEob3kasmh6cN44GHVOSoch34qCoVV0hkpJmatfNbl0yp4E7ZcZPFHSK8e9dAAMuyfhZZNwB9w9y1O6b1f1HwTa57kiHt5CsktTKHQUPNeOP2U0491K6TvBbgtsax5Tiyw3FMXfjB39EX9HgAcmeYwGEq9opEIYDEaTb88LJCUXjeoQoRDTzAjE2s3q7LPKa0nC8vXR82yrgnIVFVyFfiThbXYjsx4Txre6oCXwEWouK7RZbQnI9IykoXUhVYBTQpZF27CGc1Cw7cWj3K2Cc1xRYR941KXR97gBEsSqtdn1GKkJemVHnTiMurQvCxNM53XJvyApdxqtIbDwNiFBCxX8px32vQO1UvKQuUGYS6Skhfh5OwGbunikllmhCBjg7472XMa0T3kBiSG7PWhKLR57DsoP4Y6BcDDAZnA3quEZOUgn1O8MpET37ISBqZMTmZdMOnPFido78Vu9t4VjIxmOGFZ7ovabVExt2N9k1xr8TnHNrm79ktX1A79y3jZYxXRzuakvXc51QMOI9VRtPyuZ64X9n8hlf9lbjWXH9cIZygr8CJ2bIwBO1jXorEaXNM87rgO1JgaIOSQalSC3PNRQ7yFyDpWaCeh4L66dngvB9MltoxxEMPeTNtSajxCvdt1xz6cmmYyvukpjinx5DcYaW1BGyNDcGDaGKD47l23gmsQWgObt1AUXAfn931lKEMaYZdfKgi1TbRzIqaCrkgPuMD5PDqiRDJ1lxg6dP781Z7KPiWHTCgOYtSx7JhZW1ZIyGOcfVjg5r0VZWm3jTRINtGUDx3iyc1wsPZYvKE1BFWllJnUmFmA94dM2g51K6xDVXkRrzR3Fxo0HcV4rID1MqNOYSOrYM54tqUlpwo5ud5BHaF9tSAGJXdrz3kXPpbJg65W3ubxYg9pIOyCq9RAf63f8mDXcpqJvvoyqQwP86EQUDbLM3V29YhHLvoxkfGgb6fSWcreh64CJgXfj3hsaQAvLyt7eAnGGTim6aRngV00CXfvaNnBny7Ax1GcB4sRXbEdCfRsBRsNyYIZw7kk5ru3tBf5FYWnBkRNQtGbL9yKhHteOePTXKXRVDWkl3Ywn0KFtaqFjpZR7T7V9WyfpW7Lid6VgeSUfmjZNZo7POYWrf20C1g2XnNGQTl78Ebn0KMNuBUKZ3pQirRus8vQuJgMmSSug4igP54JGYdpbTGKMhFFpCfkVM7HoZCVduC31GMHnz7PTNkp5z5uBjg';

-- Issue no 11 and 14. Select ONLY required columns from Hive

-- If WHERE clauses are being locally executed, send all columns
-- to Hive (though in * format only)

reset hadoop_fdw.hadoop_rowscanthreshold;

select d_w_id, d_id, d_name from district where d_w_id = 5;

-- If the clauses are being sent to Hive, then ask for only
-- requested columns

set hadoop_fdw.hadoop_rowscanthreshold to 0;

select d_w_id, d_id, d_name from district where d_w_id = 5;

reset hadoop_fdw.hadoop_rowscanthreshold;

-- Issue no. 13. Create new functions to create local table based on Hive
-- Also create local view based on Hive view which gets created there first

-- Below will create a local table by the name of 'districtcopy'
select hadoop_fdw_create_table('localhive', 'district', 'districtcopy');
\d+ districtcopy
select * from districtcopy where d_w_id = 5;
drop foreign table districtcopy;

-- If third argument is not specified, it takes on the same name as the Hive
-- table name
select hadoop_fdw_create_table('localhive', 'warehouse');
\d+ warehouse
select * from warehouse;
drop foreign table warehouse;

-- Create a view on the Hive side and after that succeeds create a
-- corresponding local table
select hadoop_fdw_create_view('localhive', 'districtview', 'select * from district where d_name=''AblnEVzO''', 'dview');
\d+ dview
select * from dview;

-- Recreate the view. It should be re-created on Hive as well as locally
select hadoop_fdw_create_view('localhive', 'districtview', 'select * from district where d_name=''AblnEVzO''', 'dview');
\d+ dview
drop foreign table dview;

-- If fourth argument is not specified, it takes on the same name as the Hive
-- view name
select hadoop_fdw_create_view('localhive', 'districtview', 'select * from district where d_name=''AblnEVzO''');
\d+ districtview
drop foreign table districtview;

-- Test inserts into underlying HDFS files using Flume
-- NOTE: this requires a properly configured Flume instance
-- running before the start of this test run
-- The conf file required by Flume is available in
-- test/files/flume_hive_hdfs.conf
-- Here's how to start Flume. Change directory into your installed
-- bigsql location
-- cd bigsql-9.3prefix
-- Fire off the following command to start Flume with the above
-- conf file
--./flume/bin/flume-ng agent --conf ./flume/conf/ -f ~/path/to/this/fdw/test/files/flume_hive_hdfs.conf
-- -Dflume.root.logger=DEBUG,console -n test1
-- NOTE: This needs to be started before running "make installcheck"!!
-- 
CREATE FOREIGN TABLE district_in (
  d_w_id       integer,
  d_id         integer,
  d_ytd        numeric,
  d_tax        numeric,
  d_next_o_id  integer,
  d_name       varchar,
  d_street_1   varchar,
  d_street_2   varchar,
  d_city       varchar,
  d_state      varchar,
  d_zip        varchar
) SERVER localhive 
OPTIONS (table 'district', flume_port '44444');

-- INSERT into this table, it should use flume for this
insert into district_in values (10, 2, 30000, 0.0018, 3001, 'BeebOPness', 'FDKFKFDfGGFeeF', 'JJFDeeeszbbvf', 'GJKFDKllkGDFS', 'AF', 123456789);
-- Sleep for a while to let HDFS catch up
select pg_sleep(10);
select * from district_in where d_name = 'BeebOPness';
-- NOTE: The row will still be available via selects on the original "district" table above
drop foreign table district_in;

-- Test INSERTs, UPDATEs, DELETEs into underlying Hbase table
-- NOTE: this requires a properly configured Hbase instance
-- running before the start of this test run. The corresponding
-- thrift server also needs to be up and running
-- We need to create an Hbase mapped Hive table as well for this.
-- NOTE: The name of the mapped Hive table should be the same
-- as the underlying Hbase table. This is needed because the fdw
-- will use the same table name to query Hbase directly as well
-- NOTE: The same hbase mapping for the columns should be used
-- in the foreign table as the one present in the Hive mapped
-- table as well. Otherwise DML will not work properly at all
-- ---- PREREQUISITE ----
-- Create a hive table based on an Hbase one before starting
-- the test run. Go to the "hive" commandline and issue the below
--
-- CREATE TABLE hbase_table(key string, value string) STORED BY 'org.apache.hadoop.hive.hbase.HBaseStorageHandler' WITH
-- SERDEPROPERTIES ("hbase.columns.mapping" = ":key,cf:val") TBLPROPERTIES ("hbase.table.name" = "hbase_table");
--
-- NO need to do anything in Hbase. The above will create the underlying Hbase table as well
-- NOTE: The run can HANG if things are not setup properly

CREATE FOREIGN TABLE hive_hbase_table (
  key   varchar,
  value varchar
) SERVER localhive 
OPTIONS (table 'hbase_table', hbase_address 'localhost', hbase_port '9090', hbase_mapping ':key,cf:val');

-- INSERT into this table. This will use Hbase for the insert
INSERT INTO hive_hbase_table VALUES ('key1', 'value1');
INSERT INTO hive_hbase_table VALUES ('key2', 'value2');
SELECT * from hive_hbase_table;

-- UPDATE this table. This will use Hbase for the update
UPDATE hive_hbase_table SET value = 'update' WHERE key = 'key2';
SELECT * from hive_hbase_table;

-- DELETE from this table. This will use Hbase for the delete
DELETE FROM hive_hbase_table WHERE key='key1';
SELECT * from hive_hbase_table;
DROP FOREIGN TABLE hive_hbase_table;
Drop EXTENSION hadoop_fdw cascade;
