/*-------------------------------------------------------------------------
 *
 * hbaseclient.cpp
 *	Global Hbase Client Functions (usable as C callback functions)
 *
 * Copyright (c) 2012-2013, BigSQL Development Group
 *
 * IDENTIFICATION
 *                hadoop_fdw/hbase/hbase_client/hbaseclient.cpp
 *
 *-------------------------------------------------------------------------
 */
#include <stdio.h>
#include <unistd.h>
#include <sys/time.h>
#include <poll.h>
#include <sys/socket.h>
#include <netinet/in.h>


#include <iostream>

#include <boost/lexical_cast.hpp>
#include <protocol/TBinaryProtocol.h>
#include <transport/TSocket.h>
#include <transport/TTransportUtils.h>

#include "Hbase.h"
#include "HbaseClient.h"
#include "HbaseConnection.h"

using namespace std;
using namespace boost;
using namespace apache::thrift;
using namespace apache::thrift::protocol;
using namespace apache::thrift::transport;

using namespace apache::hadoop::hbase::thrift;

/*****************************************************************
 * Global HBase Client Functions (usable as C callback functions)
 *****************************************************************/

HbaseConnection *HbaseOpenConnection(const char *host, int port, bool framed, char *err_buf,
							size_t err_buf_len)
{
  shared_ptr<TSocket> socket(new TSocket(host, port));
  shared_ptr<TTransport> transport;

  if (framed) {
    shared_ptr<TFramedTransport> framedSocket(new TFramedTransport(socket));
    transport = framedSocket;
  } else {
    shared_ptr<TBufferedTransport> bufferedSocket(new TBufferedTransport(socket));
    transport = bufferedSocket;
  }

  shared_ptr<TBinaryProtocol> protocol(new TBinaryProtocol(transport));
  HbaseClient client(protocol);
  try {
    transport->open();
  } catch (TTransportException& ttx) {
    RETURN_FAILURE(__FUNCTION__, ttx.what(), err_buf, err_buf_len, NULL);
  } catch (...) {
    RETURN_FAILURE(__FUNCTION__,
                   "Unable to connect to Hbase server.", err_buf, err_buf_len, NULL);
  }

  HbaseConnection *conn = new HbaseConnection(client, transport);
  return conn;
}

HbaseReturn	HbaseCloseConnection(HbaseConnection *connection, char *err_buf, size_t err_buf_len)
{
  RETURN_ON_ASSERT(connection == NULL, __FUNCTION__,
                   "Hbase connection cannot be NULL.", err_buf, err_buf_len, HBASE_ERROR);
  RETURN_ON_ASSERT(connection->transport == NULL, __FUNCTION__,
                   "Hbase connection transport cannot be NULL.", err_buf, err_buf_len, HBASE_ERROR);
  try {
    connection->transport->close();
  } catch (...) {
    /* Ignore the exception, we just want to clean up everything...  */
  }
  delete connection;
  return HBASE_SUCCESS;
}

bool HbaseMutateRow(HbaseConnection *connection, char *table,
					char *rowid, char **cols,
					char **vals, int nvals,
					char *err_buf, size_t err_buf_len)
{
	const std::map<Text, Text>  dummyAttributes; // see HBASE-6806 HBASE-4658
	std::vector<Mutation> mutations;
	std::string	row(rowid);
	std::string t(table);
	int			i = 0;

	try {
		while (i < nvals)
		{
			mutations.push_back(Mutation());
			mutations.back().column = cols[i];
			mutations.back().value = vals[i];
			i++;
		}
		connection->client.mutateRow(t, row, mutations, dummyAttributes);
	} catch (...) {
		RETURN_FAILURE(__FUNCTION__,
                   "Unable to insert/update to Hbase server.", err_buf, err_buf_len, false);
	}
	return true;
}

bool HbaseDeleteRow(HbaseConnection *connection, char *table,
					char *rowid, char *err_buf,
					size_t err_buf_len)
{
	const std::map<Text, Text>  dummyAttributes; // see HBASE-6806 HBASE-4658
	std::string	row(rowid);
	std::string t(table);

	try {
		connection->client.deleteAllRow(t, row, dummyAttributes);
	} catch (...) {
		RETURN_FAILURE(__FUNCTION__,
		   "Unable to delete from Hbase server.", err_buf, err_buf_len, false);
	}
	return true;
}
