EXTENSION    = hadoop_fdw
EXTVERSION   = $(shell grep default_version $(EXTENSION).control | sed -e "s/default_version[[:space:]]*=[[:space:]]*'\([^']*\)'/\1/")

DATA         = $(filter-out $(wildcard sql/*--*.sql),$(wildcard sql/*.sql))
DOCS         = $(wildcard doc/*.md)
USE_MODULE_DB = 1
REGRESS      = hadoop_fdw 
REGRESS_OPTS = --inputdir=test --outputdir=test \
	--load-language=plpgsql --load-extension=$(EXTENSION)
MODULE_big      = $(EXTENSION)

HIVE_OBJS    = hive/hiveclient.o hive/hive_metastore_types.o hive/hive_service_types.o \
hive/TCLIService.o hive/ThriftHiveMetastore.o hive/hiveclienthelper.o \
hive/HiveResultSet.o hive/queryplan_constants.o hive/HiveColumnDesc.o \
hive/HiveRowSet.o hive/queryplan_types.o hive/TCLIService_types.o \
hive/hive_metastore_constants.o hive/hive_service_constants.o \
hive/TCLIService_constants.o hive/ThriftHive.o
 
HBASE_OBJS	 = hbase/Hbase.o hbase/HbaseClient.o hbase/Hbase_constants.o \
			   hbase/Hbase_types.o

OBJS         = src/hadoop_fdw.o src/deparse.o src/hive_funcs.o src/flume_client.o $(HIVE_OBJS) $(HBASE_OBJS)
PG_CONFIG    = pg_config

SHLIB_LINK += -L$(THRIFT_HOME)/lib -lthrift -lfb303 -lstdc++

all: hive_client hbase_client sql/$(EXTENSION)--$(EXTVERSION).sql

hive_client:
	$(MAKE) -C hive

hbase_client:
	$(MAKE) -C hbase

sql/$(EXTENSION)--$(EXTVERSION).sql: sql/$(EXTENSION).sql
	cp $< $@

clean: clean-hive-client clean-hbase-client
clean-hive-client:
	$(MAKE) -C hive clean

clean-hbase-client:
	$(MAKE) -C hbase clean

DATA_built = sql/$(EXTENSION)--$(EXTVERSION).sql
DATA = $(filter-out sql/$(EXTENSION)--$(EXTVERSION).sql, $(wildcard sql/*--*.sql))
EXTRA_CLEAN = sql/$(EXTENSION)--$(EXTVERSION).sql

PGXS := $(shell $(PG_CONFIG) --pgxs)
include $(PGXS)

# we put all the tests in a test subdir, but pgxs expects us not to
override pg_regress_clean_files = test/results/ test/regression.diffs test/regression.out tmp_check/ log/

