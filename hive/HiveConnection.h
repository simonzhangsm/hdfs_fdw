/**
 *   Copyright (c) 2012-2013, BigSQL Development Group
 *
 *   Portions Copyright (c) 2008-2012 Apache Software Foundation
 *
 **/

#ifndef __hive_connection_h__
#define __hive_connection_h__

#include "ThriftHive.h"
#include <boost/shared_ptr.hpp>
#include "TCLIService.h"
#include "transport/TTransport.h"

using namespace boost;
using namespace apache::thrift::transport;
using namespace apache::hive::service::cli::thrift;

/*************************************************************************************************
 * HiveConnection HiveConnection interface definition and subclasses
 ************************************************************************************************/

/**
 * @brief Container class for Hive database connections.
 *
 * This class stores the Hive database connection information. It was only meant to be created by
 * DBOpenConnection and destroyed by DBCloseConnection.
 *
 * @see DBOpenConnection()
 * @see DBCloseConnection()
 */
class HiveConnection {

public:
    HiveConnection(shared_ptr<TTransport> t) : transport(t)
    { }
  shared_ptr<TTransport> transport;
};

/*************************************************************************************************
 * HiveConnection1 Class Definition
 ************************************************************************************************/

/**
 * @brief Container class for HiveServer1 database connections.
 *
 * This class stores the Hive database connection information. It was only meant to be created by
 * DBOpenConnection and destroyed by DBCloseConnection.
 *
 * @see DBOpenConnection()
 * @see DBCloseConnection()
 */
class HiveConnection1: public HiveConnection {

public:
    HiveConnection1(shared_ptr<Apache::Hadoop::Hive::ThriftHiveClient> c, shared_ptr<TTransport> t) : HiveConnection(t), client(c)
    { }
  shared_ptr<Apache::Hadoop::Hive::ThriftHiveClient> client;
};

/*************************************************************************************************
 * HiveConnection2 Class Definition
 ************************************************************************************************/

/**
 * @brief Container class for HiveServer2 database connections.
 *
 * This class stores the Hive database connection information. It was only meant to be created by
 * DBOpenConnection2 and destroyed by DBCloseConnection2.
 *
 * @see DBOpenConnection2()
 * @see DBCloseConnection2()
 */
class HiveConnection2: public HiveConnection {

public:
    HiveConnection2(shared_ptr<TCLIServiceClient> c, shared_ptr<TTransport> t) : HiveConnection(t), client(c)
    { }

  shared_ptr<TCLIServiceClient> client;
  TSessionHandle* hive_session_handle;
};

#endif // __hive_connection_h__
